$('.banner-carousel').slick({
    dots:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
});


$('.hero-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.hero-sub-slider'
});

$('.hero-sub-slider').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.hero-slider',
  dots: true,
  centerMode: true,
  focusOnSelect: true
});