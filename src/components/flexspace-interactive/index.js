function defaultInteractiveAccordion(state) {
    var parentSpace = state.closest('.flex-space-interactive');
    parentSpace.toggleClass("active");
}

$('.flex-space-button').click(function(e){ 
    var clickedElement = $(this);
    defaultInteractiveAccordion(clickedElement);
})


export default defaultInteractiveAccordion;